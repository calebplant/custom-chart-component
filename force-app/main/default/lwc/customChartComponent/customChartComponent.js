import { LightningElement } from 'lwc';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import chartJsResources from '@salesforce/resourceUrl/chartJsResources';
import getReportData from '@salesforce/apex/CustomChartController.getReportData';

export default class CustomChartComponent extends LightningElement {

    reportData;
    monthlyForecastChart;
    monthlyConversionChart;

    error;
    chartjsInitialized = false;

    renderedCallback() {
        if (this.chartjsInitialized) {
            return;
        }
        this.chartjsInitialized = true;

        // Load chart.js
        Promise.all([
            loadScript(this, chartJsResources + '/Chart.bundle.min.js'),
            loadStyle(this, chartJsResources + '/Chart.min.css')
        ])
            .then(() => {
                // Run reports and get data
                getReportData()
                    .then(result => {
                        console.log('Successfully got report data');
                        console.log(JSON.parse(result));

                        this.reportData = JSON.parse(result);
                        this.renderForecastByMonthChart(this.reportData['AmountForecastByMonth'], this.reportData['RevenueForecastByMonth']);
                        this.renderConversionRateByMonthChart(this.reportData['ConversionRateByMonthWins'], this.reportData['ConversionRateByMonthLosses']);
                    })
                    .catch(error => {
                        console.log('Error getting report data');
                        console.log(error);
                    });
                
            })
            .catch((error) => {
                this.error = error;
            });
    }

    // Utils
    renderForecastByMonthChart(amountData, revenueData) {
        console.log('START render forecast by month');
        window.Chart.platform.disableCSSInjection = true;

        try{
                let sData = {
                    label: {
                                amount: [],
                                revenue: []
                            },
                    time: {
                            amount: [],
                            revenue: []
                          }
                    };

                // Amount
                amountData.forEach(eachPoint => {
                    sData.label.amount.push(new Date(eachPoint.x));
                    sData.time.amount.push(eachPoint.y);
                });
                // Revenue
                revenueData.forEach(eachPoint => {
                    sData.label.revenue.push(new Date(eachPoint.x));
                    sData.time.revenue.push(eachPoint.y);
                });
        
                let timeData = {
                    labels: sData.label.amount,
                    datasets: [
                        {
                            label: 'Potential Revenue',
                            data: sData.time.amount,
                            backgroundColor: 'rgba(200, 50, 0, 0.1)'
                        },
                        {
                            label: 'Expected Revenue',
                            data: sData.time.revenue,
                            backgroundColor: 'rgba(0, 200, 150, 0.1)'
                        }
                    ]};
                    
                console.log('timeData:');
                console.log(timeData);
        
                let timeOptions = {
                    responsive: true,
                    title: {
                        display: false
                    },
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    tooltips: {
                        callbacks: {
                        label: function(tooltipItem, data){
                            return parseInt(tooltipItem.value)
                            }
                        }
                    },
                    scales: {
                        xAxes: [{
                            type: 'time',
                            gridLines: {
                                display:true
                            },
                            time: {
                                minUnit: 'month'
                            }
                        }],

                        yAxes: [{
                            ticks: {
                                min: 0,
                                callback: function(value, index, values) {
                                    return '$' + (value / 100).toLocaleString();
                                }
                            }
                        }]
                    }
                }
        
                const forecastByMonthCanvas = document.createElement('canvas');
                this.template.querySelector('div.forecast-by-month').appendChild(forecastByMonthCanvas);
                const forecastByMonthCtx = forecastByMonthCanvas.getContext('2d');
                this.testChart = new window.Chart(forecastByMonthCtx, {
                    type: "line",
                    data: timeData,
                    options: timeOptions
            });
        } catch (err) {
            console.log(err);
        }

        console.log('STOP render forecast by month');
    }

    renderConversionRateByMonthChart(winData, lossData) {
        console.log('START render conversion rate by month');
        window.Chart.platform.disableCSSInjection = true;

        try{
                let sData = {label: {
                    win: [],
                    loss: []
                }, time: {
                    win: [],
                    loss: []
                }};

                // win
                winData.forEach(eachPoint => {
                    sData.label.win.push(new Date(eachPoint.x));
                    sData.time.win.push(eachPoint.y);
                });
                // loss
                lossData.forEach(eachPoint => {
                    sData.label.loss.push(new Date(eachPoint.x));
                    sData.time.loss.push(eachPoint.y);
                });
        
                let timeData = {
                    labels: sData.label.win,
                    datasets: [
                        {
                            label: 'Total Wins',
                            data: sData.time.win,
                            backgroundColor: 'rgba(127, 63, 191, 0.75)'
                        },
                        {
                            label: 'Total Losses',
                            data: sData.time.loss,
                            backgroundColor: 'rgba(238, 238, 32, 0.75)'
                        }
                    ]};

                console.log('timeData:');
                console.log(timeData);
        
                let timeOptions = {
                    responsive: true,
                    title: {
                        display: false
                    },
                    legend: {
                        display: true,
                        position: 'bottom'
                    },
                    tooltips: {
                        callbacks: {
                        label: function(tooltipItem, data){
                            return parseInt(tooltipItem.value)
                            }
                        }
                    },
                    scales: {
                        xAxes: [{
                            type: 'time',
                            offset: true,
                            gridLines: {
                                display:true
                            },
                            time: {
                                minUnit: 'month'
                            }
                        }],

                        yAxes: [{
                            ticks: {
                                min: 0,
                                stepSize: 1
                            }
                        }]
                    }
                }
        
                const conversionRateCanvas = document.createElement('canvas');
                this.template.querySelector('div.conversion-rate-by-month').appendChild(conversionRateCanvas);
                const conversionRateCtx = conversionRateCanvas.getContext('2d');
                this.monthlyConversionChart = new window.Chart(conversionRateCtx, {
                    type: "bar",
                    data: timeData,
                    options: timeOptions
            });
        } catch (err) {
            console.log(err);
        }

        console.log('STOP render conversion rate by month');
    }
}