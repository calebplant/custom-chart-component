@isTest
public with sharing class CustomChartController_Test {

    private static List<String> REPORT_DATASETS = new List<String>{
        'AmountForecastByMonth',
        'RevenueForecastByMonth',
        'ConversionRateByMonthWins',
        'ConversionRateByMonthLosses'
    };

    @isTest(SeeAllData='true')
    public static void doesGetReportDataReturnMap() {

        Test.startTest();
        String reportDataString = CustomChartController.getReportData();
        System.debug(reportDataString);
        Map<String, List<OpportunityChartPoint>> reportData = (Map<String, List<OpportunityChartPoint>>)JSON.Deserialize(reportDataString, Map<String, List<OpportunityChartPoint>>.Class);
        Test.stopTest();
        
        for(String eachDataset : REPORT_DATASETS) {
            System.assert(reportData.keySet().contains(eachDataset));
        }
    }
}
