public with sharing class RandUtils {

    public static Integer getRandInt(Integer max)
    {
        return Integer.valueOf(Math.random() * max);
    }

    public static Integer getRandInt(Integer min, Integer max)
    {
        return min + Integer.valueOf(Math.random() * (max - min));
    }

    public static Date getRandDate(Date maxDate)
    {
        Date today = Date.today();
        Integer maxDaysAdded = today.daysBetween(maxDate);
        return today.addDays(getRandInt(maxDaysAdded));
    }

    public static Date getRandPastDate(Date minDate)
    {
        Date today = Date.today();
        Integer maxDaysRemoved = today.daysBetween(minDate);
        return today.addDays(getRandInt(maxDaysRemoved));
    }

    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
}
