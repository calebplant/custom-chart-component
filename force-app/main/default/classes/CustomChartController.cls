public with sharing class CustomChartController {

    private static String FORECAST_BY_MONTH_NAME = 'High_Value_Opportunities_inl';
    private static String CONVERSION_RATE_BY_MONTH = 'High_Value_Opp_Won_vs_Lost_gFN';
    private static Map<String, Integer> SUMMARY_INDEX_BY_NAME = new Map<String, Integer>{
        'AmountForecastByMonth' => 0,
        'RevenueForecastByMonth' => 1,
        'ConversionRateByMonthWins' => 0,
        'ConversionRateByMonthLosses' => 1
    };

    @AuraEnabled
    public static String getReportData()
    {
        Map<String, List<OpportunityChartPoint>> pointMap = new Map<String, List<OpportunityChartPoint>>();
        List<String> reportNames = new List<String>{FORECAST_BY_MONTH_NAME, CONVERSION_RATE_BY_MONTH};

        // Get reports and map them by the report name
        List <Report> reportList = [SELECT Id,DeveloperName FROM Report WHERE 
            DeveloperName IN :reportNames];
        Map<String, Reports.reportResults> reportResultsByName = new Map<String, Reports.reportResults>();
        // Run each report synchronously
        for(Report eachReport : reportList) {
            reportResultsByName.put(eachReport.DeveloperName, Reports.ReportManager.runReport(eachReport.Id, true));
        }
        
        // Get results for Revenue forecast by month
        Reports.reportResults monthlyForecastResult = reportResultsByName.get(FORECAST_BY_MONTH_NAME);
        pointMap.put('AmountForecastByMonth', parseSummaryResults(monthlyForecastResult, SUMMARY_INDEX_BY_NAME.get('AmountForecastByMonth')));
        pointMap.put('RevenueForecastByMonth', parseSummaryResults(monthlyForecastResult, SUMMARY_INDEX_BY_NAME.get('RevenueForecastByMonth')));
        // Get results for Wins vs Losses by month
        Reports.reportResults monthlyConversionRateResult = reportResultsByName.get(CONVERSION_RATE_BY_MONTH);
        pointMap.put('ConversionRateByMonthWins', parseSummaryResults(monthlyConversionRateResult, SUMMARY_INDEX_BY_NAME.get('ConversionRateByMonthWins')));
        pointMap.put('ConversionRateByMonthLosses', parseSummaryResults(monthlyConversionRateResult, SUMMARY_INDEX_BY_NAME.get('ConversionRateByMonthLosses')));

        return JSON.serialize(pointMap);
    }

    private static List<OpportunityChartPoint> parseSummaryResults(Reports.reportResults results, Integer index)
    {
        List<OpportunityChartPoint> points = new List<OpportunityChartPoint>();
        
        for(Reports.GroupingValue eachGrouping : results.getGroupingsDown().getGroupings()) {
            // X value
            Datetime closeDate = (Datetime)eachGrouping.getValue();
            // Y value
            String factMapKey = eachGrouping.getKey() + '!T';
            Reports.ReportFactWithDetails factDetails =
                (Reports.ReportFactWithDetails)results.getFactMap().get(factMapKey);
            Reports.SummaryValue sumVal = factDetails.getAggregates()[index];
            Integer monthAmount = Integer.valueOf(sumVal.getLabel().replaceAll('[^0-9]', ''));

            OpportunityChartPoint newPoint = new OpportunityChartPoint(closeDate, monthAmount);
            points.add(newPoint);
        }
        return points;
    }
}
