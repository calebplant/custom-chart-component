public with sharing class DataFactory {
    
    public static void addRandomOpportunitiesForYear(Integer numOfOpportunities, Integer minAmount, Integer maxAmount, Date maxCloseDate)
    {
        List<String> oppStatuses = new List<String>{'Prospecting', 'Closed Won', 'Closed Lost'};
        List<Opportunity> opps = new List<Opportunity>();
        for(Integer i=0; i < numOfOpportunities; i++) {
            Opportunity newOpp = new Opportunity();
            newOpp.Amount = RandUtils.getRandInt(maxAmount);
            newOpp.StageName = oppStatuses[RandUtils.getRandInt(oppStatuses.size())];
            newOpp.CloseDate = RandUtils.getRandDate(Date.today().addYears(1));
            newOpp.Name = RandUtils.generateRandomString(10);
            newOpp.Probability = RandUtils.getRandInt(100);
            opps.add(newOpp);
        }
        insert opps;
    }

    public static void addRandomOpportunitiesForPastYear(Integer numOfOpportunities, Integer minAmount, Integer maxAmount)
    {
        List<String> oppStatuses = new List<String>{'Prospecting', 'Closed Won', 'Closed Lost'};
        List<Opportunity> opps = new List<Opportunity>();
        for(Integer i=0; i < numOfOpportunities; i++) {
            Opportunity newOpp = new Opportunity();
            newOpp.Amount = RandUtils.getRandInt(maxAmount);
            newOpp.StageName = oppStatuses[RandUtils.getRandInt(oppStatuses.size())];
            newOpp.CloseDate = RandUtils.getRandPastDate(Date.today().addYears(-1));
            newOpp.Name = RandUtils.generateRandomString(10);
            newOpp.Probability = RandUtils.getRandInt(100);
            opps.add(newOpp);
        }
        insert opps;
    }
}
