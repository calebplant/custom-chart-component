public with sharing class OpportunityChartPoint {

    @AuraEnabled
    public Datetime x;
    @AuraEnabled
    public Integer y;

    public OpportunityChartPoint(Datetime xPos, Integer yPos)
    {
        this.x = xPos;
        this.y = yPos;
    }

}
