@isTest
public with sharing class RandUtils_Test {

    @isTest
    static void doesGetRandIntReturnRandomInteger()
    {
        Integer maxVal = 10;
        Integer minVal = 2;
        Test.startTest();
        for(Integer i=0; i < 200; i++) {

            Integer noMin = RandUtils.getRandInt(maxVal);
            System.assert(noMin >= 0 && noMin <= maxVal);

            Integer withMin = RandUtils.getRandInt(minVal, maxVal);
            System.assert(withMin >= minVal && withMin <= maxVal);
        }
        Test.stopTest();
    }

    @isTest
    static void doesGetRandDateReturnRandDate()
    {
        Date maxDate = Date.today().addDays(10);
        Test.startTest();
        for(Integer i=0; i < 200; i++) {

            Date randDate = RandUtils.getRandDate(maxDate);
            System.assert(randDate >= Date.today() && randDate <= maxDate);
        }
        Test.stopTest();
    }

    @isTest
    static void doesGenerateRandomStringReturnEffectivelyRandom()
    {
        Integer stringLen = 10;
        Set<String> seenStrings = new Set<String>();

        Test.startTest();
        for(Integer i=0; i < 200; i++) {
            String randString = RandUtils.generateRandomString(stringLen);
            System.assert(!seenStrings.contains(randString));
            seenStrings.add(randString);
        }
        Test.stopTest();
    }
}
