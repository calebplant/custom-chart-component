@isTest
public with sharing class DataFactory_Test {
    
    @isTest
    static void doesAddRandomOpportunitiesForYearCreateOpportunities()
    {
        Integer numStartOpps = [SELECT COUNT() FROM Opportunity];
        Integer numNewOpps = 10;

        Test.startTest();
        DataFactory.addRandomOpportunitiesForYear(numNewOpps, 0, 100, Date.today().addDays(10));
        Test.stopTest();

        Integer numEndOpps = [SELECT COUNT() FROM Opportunity];
        System.assertEquals(numStartOpps + numNewOpps, numEndOpps);
    }
}
