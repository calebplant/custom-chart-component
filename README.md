# ChartJs Component

## Overview

Component demonstrating how to use [Chart.js](https://www.chartjs.org/) in a Lightning Component to display custom charts using report data.

## Screenshot

![Sample Charts Using Chart.Js](media/ss_1.png)

In the above exmaple, the chart on the left shows potential and expected revenue together for the next few months for high-value opportunities (>$50,000). The chart on the right shows the number of wins and losses for high-value opportunities over the past few months.

For reference, the below screenshot is an excerpt from the report that the chart pulls its data from.

![Report The Generates Data](media/report_1.png)

## File Overview

### LWC

* **customChartComponent** - Stand-alone component that displays charts made using Chart.js to better show data from salesforce reports.

### Apex

* **CustomChartController** - Controller for the Custom Chart Component. Runs reports using the apex Reporting Api and feeds the results back to the LWC to display